# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['I:\\Daten\\Studium\\Master\\Masterarbeit\\code\\pylistory'],
             binaries=[],
             datas=[
             ('resources', 'resources'),
             ('data', 'data'),
             ('C:\\Users\\Manu\\venv\\Lib\\site-packages\\pygame_menu\\resources', 'pygame_menu\\resources'),
             ],
             hiddenimports=["thorpy", "pytmx"],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='pylistory',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='main')
